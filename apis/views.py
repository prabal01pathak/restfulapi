from django.shortcuts import render,HttpResponse
from rest_framework import viewsets
from .serializers import ListSerializer
from .models import List


class ListView(viewsets.ModelViewSet):
    serializer_class = ListSerializer
    queryset = List.objects.all()
